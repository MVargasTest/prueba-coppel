<?php

use App\Http\Controllers\ClasesController;
use App\Http\Controllers\DepartamentosController;
use App\Http\Controllers\FamiliasController;
use App\Http\Controllers\ProductosController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $productos = ProductosController::getProductosController();
    echo $productos;
});

Route::get('/testdb',function(){
    $clases = ClasesController::getClasesController();
    $departamentos = DepartamentosController::getDepartamentosController();
    $familias = FamiliasController::getFamiliasController();

    echo "CLASES", $clases;
    echo "==============================================";
    echo "DEPARTAMENTOS", $departamentos;
    echo "==============================================";
    echo "FAMILIAS", $familias;
});

Route::get('/productos', function(){
    $productos = ProductosController::getProductosController();
    echo $productos;
});
Route::get('/registro-productos',  [ProductosController::class, 'registroProductos'])->name('registro-productos');

Route::post('/guardarProductos', [ProductosController::class, 'guardarProductos']);

Route::get('/checkSku', function(Request $request){
    $checkSku = ProductosController::checkSku($request);

    return $checkSku;
});

Route::get('/eliminar-producto/{id}', [ProductosController::class, 'eliminarProducto']);
Route::get('/modificar-producto/{id}', [ProductosController::class, 'modificarProducto']);
Route::post('/actualizar-productos', [ProductosController::class, 'actualizarProductos']);


Route::get('/postman/csrf', function (Request $request) {
	return csrf_token();
});
