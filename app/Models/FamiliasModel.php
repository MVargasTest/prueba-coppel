<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FamiliasModel extends Model
{
    use HasFactory;
    public function getFamiliasModel(){
        $familias = DB::table('familias')->get();

        return $familias;
    }
}
