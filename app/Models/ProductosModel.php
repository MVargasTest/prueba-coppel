<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductosModel extends Model
{
    use HasFactory;
    public function getProductosModel(){
        $productos = DB::table('productos')->get();

        return $productos;
    }
}
