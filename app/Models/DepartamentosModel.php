<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DepartamentosModel extends Model
{
    use HasFactory;
    public function getDepartamentosModel(){
        $departamentos = DB::table('departamentos')->get();

        return $departamentos;
    }
}
