<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClasesModel extends Model
{
    use HasFactory;
    public function getClasesModel(){
        $clases = DB::table('clases')->get();

        return $clases;
    }
}
