<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClasesModel;

class ClasesController extends Controller
{
    // select Clases
    public function getClasesController(){
        $print_clases = ClasesModel::getClasesModel();

        return $print_clases;
    }
}
