<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductosModel;
use App\Models\DepartamentosModel;
use App\Models\ClasesModel;
use App\Models\FamiliasModel;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class ProductosController extends Controller
{
    // select prodcutos
    public function getProductosController(){
        $print_productos = ProductosModel::getProductosModel();

        return view('productos', ['productos' => $print_productos]);
    }
    //mostrar vista registro productos
    public function registroProductos(){
        $departamentos = DepartamentosModel::getDepartamentosModel();
        $clases = ClasesModel::getClasesModel();
        $familias = FamiliasModel::getFamiliasModel();

        return view('registro', ['departamentos' => $departamentos, 'clases' => $clases, 'familias' => $familias]);

    }

    // Checar la existencia del sku
    public function checkSku(Request $request){
        //return var_dump((int)$request['sku']);
        $sku = (int)$request['sku'];
        $isset_sku = DB::select('call checkSku(?)',array($sku));
        return count($isset_sku);
    }

    public function guardarProductos(Request $request){
        $txtSku = $request->txtSku;
        $txtArticulo = $request->txtArticulo;
        $txtMarca = $request->txtMarca;
        $txtModelo = $request->txtModelo;
        $departamento = $request->departamento;
        $clase = $request->clase;
        $familia = $request->familia;
        $txtStock = $request->txtStock;
        $txtCantidad = $request->txtCantidad;
        $txtFechaAlta = $request->txtFechaAlta;
        $txtFechaBaja = $request->txtFechaBaja;

        $query = DB::select('call insertProducts(?,?,?,?,?,?,?,?,?,?,?,?)',
        array($txtSku,$txtArticulo,$txtMarca,$txtModelo,$departamento,$clase,$familia,$txtStock,$txtCantidad,0,$txtFechaAlta, $txtFechaBaja));

        $productos = ProductosController::getProductosController();

        return redirect('/productos')->with('status', 'Producto agregado');
    }

    public function eliminarProducto($id = 0){

        $query = DB::select('call eliminarProducto(?)', array($id));

        $productos = ProductosController::getProductosController();

        return redirect('/productos')->with('status', 'Producto eliminado correctamente');
    }

    public function modificarProducto($id = 0){

        $producto = DB::select('call getProductosWhere(?)', array($id));
        $departamentos = DepartamentosModel::getDepartamentosModel();
        $clases = ClasesModel::getClasesModel();
        $familias = FamiliasModel::getFamiliasModel();

        //return var_dump($producto);



        return view('actualizar', ['productos' => $producto,'departamentos' => $departamentos, 'clases' => $clases, 'familias' => $familias]);
    }

    public function actualizarProductos(Request $request){
        $txtSku = $request->txtSku;
        $txtArticulo = $request->txtArticulo;
        $txtMarca = $request->txtMarca;
        $txtModelo = $request->txtModelo;
        $departamento = $request->departamento;
        $clase = $request->clase;
        $familia = $request->familia;
        $txtStock = $request->txtStock;
        $txtCantidad = $request->txtCantidad;
        $descontinuado = $request->checkDescontinuado;
        $txtFechaBaja = $request->txtFechaBaja;

        $query = DB::select('call updateProduct(?,?,?,?,?,?,?,?,?,?,?)',
        array($txtSku,$txtArticulo,$txtMarca,$txtModelo,$departamento,$clase,$familia,$txtStock,$txtCantidad,$descontinuado, $txtFechaBaja));

        $productos = ProductosController::getProductosController();

        return redirect('/productos')->with('status', 'Producto actualizado correctamente');
    }
}
