<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DepartamentosModel;

class DepartamentosController extends Controller
{
    // select Clases
    public function getDepartamentosController(){
        $print_departamentos = DepartamentosModel::getDepartamentosModel();

        return $print_departamentos;
    }
}
