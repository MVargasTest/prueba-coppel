<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FamiliasModel;

class FamiliasController extends Controller
{
    // select Familias
    public function getFamiliasController(){
        $print_familias = FamiliasModel::getFamiliasModel();

        return $print_familias;
    }
}
